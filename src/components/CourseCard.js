import { useState } from 'react';

//bootstrap
import { Row, Col, Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CourseCard({courseProp}){

	//alternatives to do props
	//1. props as parameter then in getting the data should be props.courseProp.(specific part of information)
	//2. destructure using {} parameters should be {courseProp} 

	//checks to see if the data was successfully passed
	// console.log(courseProp)
	// console.log(typeof courseProp)

	//Deconstruct the course properties into their own variables(destructuring)
	const {name, desc, price} = courseProp

	//use the state hook for this component to be able to store its state
	//States are used to keep track of information related to individual component
	//Syntax:
		/*
			const[getter, setter] = useState(initialGetterValue)
		*/
		//getter = store initial(default value)
		//setter = updated value

	const [count, setCount] = useState(0)
	const [availSeats, maxSeats] = useState(10)

	console.log(useState(0))

	const enroll = () => {

		if(availSeats <= 0){
			alert("No more seats available")
		}else{
			setCount(count + 1);
			maxSeats(availSeats - 1);
		}
	}


	return(
		<Row>
			<Col xs={12} md={12}>
				<Card>
				  <Card.Header><h2>{name}</h2></Card.Header>
				  <Card.Body>
				    <Card.Subtitle>Description</Card.Subtitle>
				    <Card.Text>
				      {desc}
				    </Card.Text>
				    <Card.Subtitle>Price:</Card.Subtitle>
				    <Card.Text>{price}</Card.Text>
				    <Card.Text>Enrollees: {count}</Card.Text>
				    <Card.Text>Seats: {availSeats}</Card.Text>
				    <Button variant="primary" onClick={enroll}>Enroll</Button>
				  </Card.Body>
				  <Card.Footer className="text-muted">Updated 2 days ago</Card.Footer>
				</Card>
			</Col>
		</Row>
		)
}

//validation check if the CourseCard component is getting the correct prop types
//Prop types are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next.
CourseCard.propTypes = {
	//The 'shape' method is used to check if a prop object conforms to a specific 'shape'
	courseProp: PropTypes.shape({
		//define the properties and their expected types
		name: PropTypes.string.isRequired,
		desc: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}